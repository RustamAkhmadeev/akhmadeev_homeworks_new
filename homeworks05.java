import java.util.Arrays;

public class Main {

        public static void selectionSort(int[] array) {
            for (int i = 0; i <array.length; i ++) {
                int min = array[i];
                int minIndex = i;
                for (int j = i; j < array.length; i++) {
                    if (array[j] < min) {
                        min = array[j];
                        minIndex = j;
                    }
                }
                int temp = array[i];
                array[i] = array[minIndex];
                array[minIndex] = temp;

                System.out.println(Arrays.toString(array));
            }
        }

        public static boolean search(int[]array, int element){
            for (int i = 0; i <array.length; i++) {
                if (array[i] == element) {
                    return true;
                }
            }
            return false;
        }

        public static void main(String[] args) {
            int[] a = {345, 298, 456, -1};
            selectionSort(a);
        }
}
