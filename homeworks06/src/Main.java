//Реализовать функцию, принимающую на вход массив и целое число. Данная функция должна вернуть индекс
// этого числа в массиве.
//Если число в массиве отсутствует - вернуть -1.
//Реализовать процедуру, которая переместит все значимые элементы влево, заполнив нулевые, например:
//было:
//34, 0, 0, 14, 15, 0, 18, 0, 0, 1, 20
//стало
//34, 14, 15, 18, 1, 20, 0, 0, 0, 0, 0

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        Integer[] arr = {34, 0, 0, 14, 15, 0, 18, 0, 0, 1, 20};

        List<Integer> greaterThanZeroArr = new ArrayList<>();
        List<Integer> zerosArr = new ArrayList<>();

        for (Integer element:
            arr) {
            if (element != 0) {
                greaterThanZeroArr.add(element);
            } else {
                zerosArr.add(element);
            }
        }
        List<Integer> result = new ArrayList<>(greaterThanZeroArr.size() + zerosArr.size());
        result.addAll(greaterThanZeroArr);
        result.addAll(zerosArr);

        System.out.println(result);
    }

}
