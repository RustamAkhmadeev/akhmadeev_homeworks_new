//homeworks03
//Пусть есть последовательность чисел
//a0, a1, a2, ..., aN
//N -> infinity, aN -> -1
//Описать решение следующей задачи (аналогично описанию на уроке):
//Вывести количество локальных минимумов:
//ai - локальный минимум, если ai-1 > ai < ai+1
//46
//23 -> локальный минимум
//55
//120
//33 -> локальный минимум
//400
//21 -> локальный минимум
//500
//Ответ: 3


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        List<Integer> seguence = Arrays.asList(46, 23, 55, 120, 33, 400, 21, 500);
        List<Integer> localMins = new ArrayList<>();

        for (int i = 0; i < seguence.size(); i++) {
            if (i > 0) {
                if (seguence.get(i - 1) > seguence.get(i) && seguence.get(i) < seguence.get(i + 1) ) {
                    localMins.add(seguence.get(i));
                }
            }
        }

        System.out.println(localMins);
    }
}
