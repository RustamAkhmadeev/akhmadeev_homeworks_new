package Controller;
import models.Goods;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;



@Controller
public class GoodsController {

    @Autowired
    private GoodRepository goodsRepository;

    @PostMapping("/goods")
    public String addGoods (@RequestParam("description") String description,
                            @RequestParam("price") float price, @RequestParam("quantity")Integer quantity){

        Goods goods = Goods.builder()
                .description(description)
                .price(price)
                .quantity(quantity)
                .build();

        goodsRepository.save(goods);

        return "redirect:/goods_add.html";
    }
}
