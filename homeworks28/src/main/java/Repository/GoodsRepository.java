package Repository;

import models.Goods;

import java.util.List;

public interface GoodsRepository {
    List<Goods> findAll();
    void save(Goods goods);
    List<Goods> findAllByPrice(double price);
    List<Goods> findAllByOrdersCount(int ordersCount); //* - найти все товары по количеству заказов,
    // в которых они участвуют
}
