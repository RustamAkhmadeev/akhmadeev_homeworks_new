package Repository;

import models.Goods;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.util.List;
@Component
class GoodsRepositoryJdbcTemplateImpl implements GoodsRepository {
    //language=SQL
    private static final String SQL_INSERT = "insert into goods(description, price, quantity) values (?,?,?)";

    //language=SQL
    private static final String SQL_SELECT_ALL = "select * from goods order by id_goods";

    //language=SQL
    private static final String SQL_SELECT_BY_PRICE = "select * from goods where price = ?";

    //language=SQL
    // найти все товары по количеству заказов, в которых они участвуют
    private static final String SQL_SELECT_BY_COUNT_ORDER = "select * from goods group by id_goods having id_goods in " +
            "(select goods_id from ordering group by goods_id having count(id_order) = ?)";


    private JdbcTemplate jdbcTemplate;

    @Autowired
    public GoodsRepositoryJdbcTemplateImpl(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate (dataSource);
    }

    private static final RowMapper <Goods> goodsRowMapper = (row, rowNumber) -> {

        int id_goods = row.getInt("id_goods");
        String description = row.getString("description");
        float price = row.getFloat("price");
        int quantity = row.getInt("quantity");

        return new Goods(id_goods,description,price,quantity);
    };


    @Override
    public List<Goods> findAll() {
        return jdbcTemplate.query(SQL_SELECT_ALL, goodsRowMapper);
    }

    @Override
    public void save(Goods goods) {
        jdbcTemplate.update(SQL_INSERT, goods.getDescription(), goods.getPrice(), goods.getQuantity());
    }

    @Override
    public List<Goods> findAllByPrice(double price) {

        return jdbcTemplate.query(SQL_SELECT_BY_PRICE, goodsRowMapper, price);
    }

    @Override
    public List<Goods> findAllByOrdersCount(int ordersCount) {

        return jdbcTemplate.query(SQL_SELECT_BY_COUNT_ORDER, goodsRowMapper, ordersCount);
    }
}
