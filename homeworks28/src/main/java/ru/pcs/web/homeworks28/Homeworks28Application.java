package ru.pcs.web.homeworks28;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Homeworks28Application {

    public static void main(String[] args) {
        SpringApplication.run(Homeworks28Application.class, args);
    }

}
