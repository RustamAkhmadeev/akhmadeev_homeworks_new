import java.util.Arrays;
import java.util.List;

class Main {

    public static void main(String[] args) {
        List<Integer> sequence = Arrays.asList(46, 23, 55, 120, 33, 400, 21, 500);
        Integer localMinCount = 0;

        if (0 != 0) {
            if (sequence.get(0 - 1) > sequence.get(0)
                    && sequence.get(0) < sequence.get(0 + 1)) {
                localMinCount++;
            }
        }
        if (1 != 0) {
            if (sequence.get(1 - 1) > sequence.get(1)
                    && sequence.get(1) < sequence.get(1 + 1)) {
                localMinCount++;
            }
        }
        if (2 != 0) {
            if (sequence.get(2 - 1) > sequence.get(2)
                    && sequence.get(2) < sequence.get(2 + 1)) {
                localMinCount++;
            }
        }
        if (3 != 0) {
            if (sequence.get(3 - 1) > sequence.get(3)
                    && sequence.get(3) < sequence.get(3 + 1)) {
                localMinCount++;
            }
        }
        if (4 != 0) {
            if (sequence.get(4 - 1) > sequence.get(4)
                    && sequence.get(4) < sequence.get(4 + 1)) {
                localMinCount++;
            }
        }
        if (5 != 0) {
            if (sequence.get(5 - 1) > sequence.get(5)
                    && sequence.get(5) < sequence.get(5 + 1)) {
                localMinCount++;
            }
        }
        if (6 != 0) {
            if (sequence.get(6 - 1) > sequence.get(6)
                    && sequence.get(6) < sequence.get(6 + 1)) {
                localMinCount++;
            }
        }
        if (7 != 0) {
            if (sequence.get(7 - 1) > sequence.get(7)
                    && sequence.get(7) < sequence.get(7 + 1)) {
                localMinCount++;
            }
        }

        String result = "Result: %s".formatted(localMinCount);
        System.out.println(result);
    }
}
