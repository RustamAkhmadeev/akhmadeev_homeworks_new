public class Ellipse extends Figure {
    protected double a;
    private double b;

    public Ellipse(double x, double y, double a, double b) {
        super(x, y);
        if (a > 0 && b > 0) {
            this.a = a;
            this.b = b;
        }
    }

    public double getPerimeter() {
        return 4 * (Math.PI * a * b + (a - b) * (a - b)) / (a + b); }

    }